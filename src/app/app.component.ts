import { NgModule } from '@angular/core';
import { Component } from '@angular/core';
import { SharesService } from './topic/share-service';

@NgModule({
  imports: []
})

@Component({
  selector: 'pm-root',
  
  template: `
  <div><h1>{{pageTitle}}</h1>
      <pm-products></pm-products>
  </div>
  `,
  providers: [SharesService]
})
export class AppComponent { }