import { DecimalPipe } from "@angular/common";

export interface IShare {
    certificateId: number;
    numberOfShares: number;
    issuedDate: Date;
    quantity:number;
    subtotal:number;
}
