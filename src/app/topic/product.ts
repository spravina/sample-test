import { DecimalPipe } from "@angular/common";

export interface Iproduct {
    name: string;
    symbol: string;
    value: number;
    }
