import { Component,OnInit,Input  } from '@angular/core';
import { NgModule } from '@angular/core';

import { Title } from '@angular/platform-browser';
import { map } from 'rxjs/operators';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { SharesService } from './share-service';
import { IShare } from './share';
import { Iproduct } from './product';

@Component({
    selector: 'pm-products',
    templateUrl: './share-list.component.html',
    
        
    styles: [`
    table .collapse.in {
        display:table-row;
    }
    .adiv {
        border-radius: 25px;
        background: #73AD21;
        padding: 20px; 
        width: 200px;
        height: 20px; 
    }
    .button {
        background-color: #C0C0C0;
        border: none;
        color: #585050;
        padding: 10px;
        height:40px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 13px;
        margin: 6px 6px;
        border-radius: 15px;
        
    }
    .btn:focus,.btn:active {
                background-color: #2171b5 !important;
        color:white;
        border-radius: 15px;
     }
    .active {
        background-color: #2171b5 !important;
        color:white;
      }
  
    `],
    
    
})

export class shareListComponent implements OnInit { 
    selTopic: string;
    allTopics: IShare[] =  [];
    products:Iproduct[]=[];
    
    pageTitle: string = 'Share Trading';
    selectedTitle: string = '<div>Heloo</div>';
    errorMessage: string;
    
    
    SearchValue :string='';
    
    
    shareToSell: IShare[] =  [];

    tempSubTopic:string[]=[];
    tempSubTotal:number[]=[];
    
    totalShare:number=0;
    isSearch=false;
   
    currentPrice:number=0;
    totalSoldPrice:number=0;
    totalSoldQty:number=0;
    tempQtyist:number[]=[];
    buttonDisabled:boolean;
    productName:string='';
    productPrice:number=0;
    warningMsg:string='';

    calling = false;
    subTopicList = new Array();
    
   



    //search 
    calculate(val)
    {
        alert(val);
        this.totalShare=val *100;
        
    }
    
    getSearchResult()
    {
        

    Observable
    .interval(30000)
    .timeInterval()
    .flatMap(() => this._topicService.getShareDetails(this.SearchValue))
    .subscribe(data => {

        console.log(data);
       
       this.productName=data["name"];
       
        this.productPrice=data["value"];
    });
    

    }

    getCertificateList(){
        this._topicService.getTopics()
        .subscribe(topics => {
            this.allTopics = topics;
            for(let i = 0; i < this.allTopics.length; i++){
                this.totalShare= this.totalShare+this.allTopics[i].numberOfShares;
               
                //console.log(this.subTopicList);
            }

        },
        error => this.errorMessage = <any>error);

    }


    postCertificate()
    {
        this.calling = true;

        if(this.shareToSell.length > 0){
            for(let i = 0; i < this.shareToSell.length; i++){
                delete this.shareToSell[i].issuedDate;
                console.log(this.shareToSell[i]);

              
              var response=  this._topicService.sellStockRequest(this.shareToSell[i]);
              console.log(response);
            }

        }
        this.calling = false;

    }


    countPrice(topic)
    {
        
        console.log(topic);
        this.warningMsg="";
        this.tempSubTotal.push(topic.quantity * this.productPrice);
        this.tempQtyist.push(topic.quantity);
        if(topic.quantity > topic.numberOfShares)
        {
            topic.quantity=0;
            alert("quantity can not be greater then total shares")
            return;
        }
        this.shareToSell.push(topic);
        
        this.totalSoldPrice = this.tempSubTotal.reduce(function(a, b){ return a + b; }); 
        this.totalSoldQty = this.tempQtyist.reduce(function(x, y){ return x + y; }); 
        if((this.totalSoldQty *100)/this.totalShare> 50)
        {
            this.warningMsg="You are selling more than 50% of your shares!!!"
        }
        this.buttonDisabled=false;
        console.log(this.buttonDisabled);
    }



    constructor(private _topicService: SharesService) {
       
    }

    

    getSearchResult1()
    {
     // alert(this.SearchValue);
    // this.isSearch=true;
     this.calling = true;
        this._topicService.getShareDetails(this.SearchValue)
        .subscribe(data => {
           this.products = data;
           
           console.log(data);

         if(data != undefined && data !== null){
                
              this.productName=data["name"];
              
               this.productPrice=data["value"];
                    
            
          }
          this.getCertificateList();
          this.calling = false;

   
        },
        error => this.errorMessage = <any>error);
    }
    

    
       

    ngOnInit(): void {
    
        this.buttonDisabled = true;
        this.getSearchResult1();
        this.getSearchResult();
        
    }
    
}
