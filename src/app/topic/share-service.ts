import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { map } from "rxjs/operators";
import { HttpHeaders, HttpParams } from '@angular/common/http'; 
import { Iproduct } from '../topic/product';
import { IShare } from '../topic/share';



const headers = new HttpHeaders()
  .append('Content-Type', 'application/json')
  .append('Accept', 'application/json'); 

@Injectable()

export class SharesService {
    private _productUrl = 'http://developerexam.equityplansdemo.com/test/fmv';
    private _serviceUrl = 'http://developerexam.equityplansdemo.com/test/sampledata';
    private _postUrl='https://webhook.site/ff5cb7ca-3dd0-407c-9720-d1cd4628b9ad';

    constructor(private _http: HttpClient) { }


    
    getTopics(): Observable<IShare[]> {
        return this._http.get<IShare[]>(this._serviceUrl)
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    
    getShareDetails(topicName: any): Observable<Iproduct[]> {
        return this._http.get<Iproduct[]>(this._productUrl)
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

   
    private handleError(err: HttpErrorResponse) {
        console.error(err.message);
        return Observable.throw(err.message);
    }
   

    sellStock(share: IShare): Observable<IShare> {
        return this._http.post<IShare>(this._postUrl, share);
   } 


   sellStockRequest(share:IShare): void {
       console.log('stock' +JSON.stringify(share) )
       this._http
      .post(this._postUrl,
          JSON.stringify(share))
      .subscribe((res: Response) => {
        console.log(res);
        return res;
      }, err=>console.log(err));
  }
}
